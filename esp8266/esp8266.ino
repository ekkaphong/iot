#include <ESP8266WiFi.h>    
#include <ESP8266HTTPClient.h>   
#include <ArduinoJson.h> 
#include <FirebaseArduino.h>


//******** EasyScheduler ********
#include <EasyScheduler.h>

//******** Config Wifi and set Base_Url Api **********
#define WIFI_SSID "Aekaphong"
#define WIFI_PASSWORD "12345678"
#define BASE_URL "http://scit58.com/dispenserv2/public/api"

#define FIREBASE_HOST "porject-1571.firebaseio.com"
#define FIREBASE_KEY "N84xZKJomMuD3F5bEHrjAbnQD61Uf0QR29OfbScX"

#define MACHINEID "-LwHkqH4dz2keOaH_3HW"

//******** Variable for fetch Data **********
String oldData = "";
String newData = "";
String st = "";

//StaticJsonBuffer<200> msgBuffer;
DynamicJsonBuffer jsonBuffer;
Schedular Task1,Task2;
HTTPClient http;
int full = 100;
int a1,a2,a3;
void setup() {
  Serial.begin(115200);
  Task1.start();
  Task2.start();
  connectWiFi();
  Firebase.begin(FIREBASE_HOST, FIREBASE_KEY);
  oldData = getOrder();
  Serial.println(oldData);
  int demo = Firebase.getInt("/-LwHkqH4dz2keOaH_3HW/state/state");
  Firebase.setInt("/-LwHkqH4dz2keOaH_3HW/water/-LwItklYl8Nx9QMzQbCX/quantity",full);
  Firebase.setInt("/-LwHkqH4dz2keOaH_3HW/water/-LwItt6cwbZ3173hMZ2b/quantity",full);
  Firebase.setInt("/-LwHkqH4dz2keOaH_3HW/water/-LwIu1yd0aUHuKDsfr4J/quantity",full);
  Firebase.setInt("/-LwHkqH4dz2keOaH_3HW/cup/-LwI55yymVUdDA-U2P1x/quantity",full);
  Serial.println(String(demo));
  delay(2000);
}

void loop(){
  Task2.check(progress,500);
  Task1.check(getBuy,1000);
  
}

//******** getBuy function ********
void getBuy(){
  newData = getOrder();
  if(newData == oldData){
    Serial.println("no-order");  
  }else{
    JsonObject& msg = jsonBuffer.parseObject(newData);
    if (!msg.success()){
      Serial.println("parseObject() Failed!");
      return;
    }
    String msgName = msg["orderId"];
    Serial.println(msgName);
    checkDrink(msgName);
    oldData = newData;
  }
}



//******** Return GetOrder Function *********
String getOrder(){
  if (WiFi.status() == WL_CONNECTED) {
    http.begin(BASE_URL+String("/getorder/")+MACHINEID);  
    int httpCode = http.GET();                                                                  
      if (httpCode > 0) { 
        String payload = http.getString();
        return payload;  
      }
    http.end();
  }
}

//******** Static ConnectWifi Function *********
void connectWiFi(){
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("Connected to ");
  Serial.println(WIFI_SSID);
  Serial.print("IP address: "); 
  Serial.println(WiFi.localIP());
}

void checkDrink(String id){
  if( id == "-LwItklYl8Nx9QMzQbCX" ){
    Serial.println("[1]");
  }else if(id == "-LwItt6cwbZ3173hMZ2b"){
    Serial.println("[2]");
  }
  else if(id == "-LwIu1yd0aUHuKDsfr4J"){
    Serial.println("[3]");
  }
}

//********** precess function **********

void progress(){
  while (Serial.available()) {
    char inChar = Serial.read();
    if (inChar == 'N') {
      Firebase.setInt("/-LwHkqH4dz2keOaH_3HW/state/state",1);
    }
    else{
      Serial.println(inChar);
      Serial.println("Wrong");
    }
  }
}
