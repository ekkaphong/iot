#include <Stepper.h>

#define HAND1  31
#define HAND2  33
#define DRINK1  2
#define DRINK2  3
#define DRINK3  4
#define A1      5
#define A2      6
#define LIMIT   7
#define DIRPIN  8
#define STEPPIN 9
#define ENABLE  10
#define TANK1   11   
#define TANK2   12
#define WAIT    500
#define WAITSCUCCESS   1000
#define WAITWATER   9000
#define POS1    17
#define POS2    12
#define STEPPERREV 200 //Change according to stepper motor type NEMA17 (200steps/1.8Deg)

String buyData = "";
Stepper myStepper(STEPPERREV, DIRPIN, STEPPIN);

void setup() {
  Serial.begin(115200);
  Serial3.begin(115200);
  Serial.setTimeout(100);
  Serial3.setTimeout(100);
  
  pinMode(ENABLE, OUTPUT);  //Stepper enable
  pinMode(DRINK1,OUTPUT);   //Pump1     (Active LOW)
  pinMode(DRINK2,OUTPUT);   //Pump2     (Active LOW)
  pinMode(DRINK3,OUTPUT);   //Pump3     (Active LOW)
  pinMode(HAND1,OUTPUT);   //HAND1     
  pinMode(HAND2,OUTPUT);   //HAND2 
  pinMode(A1,OUTPUT);       //LED (Active LOW)
  pinMode(A2,OUTPUT);       
  pinMode(LIMIT, INPUT);    //Limit Switch
  pinMode(TANK1, INPUT);    //Tank1 Volumn
  pinMode(TANK2, INPUT);    //Tank2 Volumn 
  digitalWrite(ENABLE, LOW);  
  digitalWrite(A1,LOW);     //LED OFF
  digitalWrite(A2,LOW);
  digitalWrite(DRINK1,LOW);
  digitalWrite(DRINK2,LOW);
  digitalWrite(DRINK3,LOW);
  myStepper.setSpeed(850);
  while(digitalRead(LIMIT)==LOW){     //Move a plate to start position ultil it hits a limit switch
    Serial.println("moving to start");
    myStepper.step(-STEPPERREV);
  }
  setHand();
  delay(2000);
}

void loop() {
//  drinking(2);
}

void drinking(int drinkType){
  switch (drinkType){ //Dispense drink after choosing
  case 1:
      digitalWrite(A1, HIGH); //LED ON
      pushCup();
        //********** Move a cup into position **********
      if(digitalRead(TANK1)== LOW){
        for(int i=0;i<=POS1;i++){
          myStepper.step(STEPPERREV);
        }
        delay(WAIT);
        
        //********** Dispense Drink **********
        digitalWrite(DRINK1, HIGH); 
        progressbar();
//        delay(WAITWATER);                                              
        digitalWrite(DRINK1, LOW);
        delay(WAIT);
    
        //********** Move a plate to start position **********
        for(int i=0;i<=POS2;i++){
          myStepper.step(STEPPERREV);
        }
  
        while(digitalRead(TANK2)==LOW){       
          delay(1000);
        }
        if(digitalRead(TANK2)== HIGH) {
          delay(WAITSCUCCESS); 
          while(digitalRead(LIMIT)==LOW){
            myStepper.step(-STEPPERREV);
          }
        delay(WAIT);
        digitalWrite(A1, LOW);  //LED OFF
        delay(WAIT);
        }
      }
      else{
        Serial.println("ไม่มีแก้วน้ำ");  
      }
      break;
  case 2:
      digitalWrite(A1, HIGH); //LED ON
      pushCup();
        //********** Move a cup into position **********
      if(digitalRead(TANK1)== LOW){
        for(int i=0;i<=POS1;i++){
          myStepper.step(STEPPERREV);
        }
        delay(WAIT);
        
        //********** Dispense Drink **********
        digitalWrite(DRINK2, HIGH);
        progressbar();
//        delay(WAITWATER);                                                            
        digitalWrite(DRINK2, LOW);
        delay(WAIT);
    
        //********** Move a plate to start position **********
        for(int i=0;i<=POS2;i++){
          myStepper.step(STEPPERREV);
        }
  
        while(digitalRead(TANK2)==LOW){       
          delay(1000);
        }
        if(digitalRead(TANK2)== HIGH) {
          delay(WAITSCUCCESS); 
          while(digitalRead(LIMIT)==LOW){
            myStepper.step(-STEPPERREV);
          }
        delay(WAIT);
        digitalWrite(A1, LOW);  //LED OFF
        delay(WAIT);
        }
      }
      else{
        Serial.println("ไม่มีแก้วน้ำ");  
      }
      break;
   case 3:
      digitalWrite(A1, HIGH); //LED ON
      pushCup();
        //********** Move a cup into position **********
      if(digitalRead(TANK1)== LOW){
        for(int i=0;i<=POS1;i++){
          myStepper.step(STEPPERREV);
        }
        delay(WAIT);
        
        //********** Dispense Drink **********
        digitalWrite(DRINK3, HIGH);
        progressbar(); 
//        delay(WAITWATER);                                                              
        digitalWrite(DRINK3, LOW);
        delay(WAIT);
    
        //********** Move a plate to start position **********
        for(int i=0;i<=POS2;i++){
          myStepper.step(STEPPERREV);
        }
  
        while(digitalRead(TANK2)==LOW){       
          delay(1000);
        }
        if(digitalRead(TANK2)== HIGH) {
          delay(WAITSCUCCESS); 
          while(digitalRead(LIMIT)==LOW){
            myStepper.step(-STEPPERREV);
          }
        delay(WAIT);
        digitalWrite(A1, LOW);  //LED OFF
        delay(WAIT);
        }
      }
      else{
        Serial.println("ไม่มีแก้วน้ำ");  
      }
      break;
  }
}

void setHand(){
  digitalWrite(HAND1, LOW);
  digitalWrite(HAND2, HIGH);
  delay(4000); 
  digitalWrite(HAND1, HIGH);
  digitalWrite(HAND2, LOW);
  delay(3400);
  digitalWrite(HAND1, LOW);
  digitalWrite(HAND2, LOW);
}

void pushCup(){
  digitalWrite(HAND1, LOW);
  digitalWrite(HAND2, HIGH);
  delay(3600); 
  digitalWrite(HAND1, HIGH);
  digitalWrite(HAND2, LOW);
  delay(3400);
  digitalWrite(HAND1, LOW);
  digitalWrite(HAND2, LOW);
}

void serialEvent3() {
  while (Serial3.available()) {
    char inChar = Serial3.read();
//    Serial.write(inChar);
    buyData += inChar;
    if (inChar == ']') {
      if (buyData.indexOf("[1]")>0) {
        drinking(1);
      }
      else if (buyData.indexOf("[2]")>0) {
        drinking(2);
      }
      else if (buyData.indexOf("[3]")>0) {
        drinking(3);
      }
      else{
        Serial.println("Wrong");
      }
        buyData = "";
    }
  }
}

void progressbar(){
    Serial.print('N');
    Serial3.print('N');
    if (Serial3.available()) { 
        Serial.print(Serial3.read());   // read it and send it out Serial (USB)
        delay(8000);
    }
}
